var scrollsteps = 40;
var scrolltmr = 0;
var scrollstartpos = 0;
var scrolli = 0;
var scrollendpos = 0;

doc = document;

doc.addEventListener("scroll", updateScroll);

window.onload = function loaded() { // When the page finishes loading
  for (let i = doc.getElementById("side-nav").children.length-1; i >= 0; i--) {
    for (let inner of doc.getElementById(i+"-section").children) {
      inner.classList.add("hidden");
    }
  }
}

function scrollToSection(element) { // High level; call to smooth scroll page to an element
  triggerScroll(doc.getElementById(element).getBoundingClientRect().top + window.scrollY - window.innerHeight * 0.3)
}

function triggerScroll(scrollEnd) { // Scroll to "scrollEnd"
  scrollstartpos = window.scrollY;
  scrolli = 0;
  scrollendpos = scrollEnd;
  scroll();
}

function scroll() { // Recusive function, only call through "triggerScroll"
  window.scrollTo(0, getScrollValue(scrollstartpos, scrollendpos, scrolli, scrollsteps));
  scrolli ++;
  if (scrolli < scrollsteps) {
    setTimeout(scroll,1);
  }
}

function getScrollValue(frompos, topos, currentStep, totalSteps) { // Get the value to scroll to next
  var radius = (frompos - topos) / 2;
  return radius * Math.cos(Math.PI / totalSteps * currentStep) + radius + topos;
}

function updateScroll() { // Updates elements that change depending on where the user is scrolled to
  if (window.scrollY > 400) {
    doc.getElementById("button-to_top").classList.remove("button_off");
    doc.getElementById("header-block").classList.add("header-block-minimise");
  }
  else {
    doc.getElementById("button-to_top").classList.add("button_off");
    doc.getElementById("header-block").classList.remove("header-block-minimise");
  }


  for (var i = doc.getElementById("side-nav").children.length-1; i >= 0; i--) {
    currentobj = doc.getElementById(i+"-section");

    if (currentobj.getBoundingClientRect().top <= window.innerHeight * 0.95) {
      for (let inner of currentobj.children) {
        if (inner.getBoundingClientRect().top <= window.innerHeight * 0.95) {
          inner.classList.remove("hidden");
        }
      }
      //doc.getElementById(i+"-section").classList.remove('hidden');
    } else {
      for (let inner of currentobj.children) {
        if (!inner.classList.contains("hidden")){
          inner.classList.add("hidden");
        }
      }
      //doc.getElementById(i+"-section").classList.add('hidden');
    }
    if (currentobj.getBoundingClientRect().top <= window.innerHeight * 0.4) {
      doc.getElementById("side-nav").children[i].classList.add("active_side");

      for (var j = doc.getElementById("side-nav").children.length-1; j >= 0; j--) {
        if (j != i) {
          doc.getElementById("side-nav").children[j].classList.remove("active_side");
        }
      }
      break;
    } else {
      if (doc.getElementById("side-nav").children[i].classList.contains("active_side")){
        doc.getElementById("side-nav").children[i].classList.remove("active_side");
      }
    }
  }

}
